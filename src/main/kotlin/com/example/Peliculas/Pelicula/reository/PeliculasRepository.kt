package com.example.Peliculas.Pelicula.reository

import com.example.Peliculas.Pelicula.model.PeliculasModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PeliculasRepository : JpaRepository<PeliculasModel, Long> {

}