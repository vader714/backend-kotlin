package com.example.Peliculas.Pelicula.model

import lombok.Data
import javax.persistence.*

@Data
@Entity
@Table(name = "PELICULAS")
class PeliculasModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    var id: Long? = null

    @Column(name = "NOMBRE")
    var nombre: String? = null

    @Column(name = "DESCRIPCION")
    var descripcion: String? = null

}