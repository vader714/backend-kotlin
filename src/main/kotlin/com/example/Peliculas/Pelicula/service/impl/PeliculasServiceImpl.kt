package com.example.Peliculas.Pelicula.service.impl

import com.example.Peliculas.Pelicula.model.PeliculasModel
import com.example.Peliculas.Pelicula.reository.PeliculasRepository
import com.example.Peliculas.Pelicula.service.PeliculasService
import org.springframework.stereotype.Service

@Service
class PeliculasServiceImpl(val peliculasRepository: PeliculasRepository) : PeliculasService {

    override fun savePelicula(pelicula: PeliculasModel): String {

        peliculasRepository.save(pelicula)
        return "guardado"
    }

    override fun updatePelicula(pelicula: PeliculasModel):String{

        peliculasRepository.save(pelicula);
        return "actualizado"
    }

    override fun getPelicula():List<PeliculasModel>{
        return peliculasRepository.findAll()

    }

    override fun deletePelicula(id:Long):String{
        peliculasRepository.deleteById(id)
        return "eliminado"
    }


}