package com.example.Peliculas.Pelicula.service

import com.example.Peliculas.Pelicula.model.PeliculasModel

interface PeliculasService {

    fun savePelicula(pelicula: PeliculasModel): String

    fun updatePelicula(pelicula: PeliculasModel): String
    fun getPelicula(): List<PeliculasModel>
    fun deletePelicula(id: Long): String
}